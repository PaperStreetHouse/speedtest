import subprocess, json
from influxdb import InfluxDBClient
def main():
    bashCommand = "speedtest-cli --bytes --json"
    process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()
    data = json.loads(output)
    print(data)
    client = InfluxDBClient("localhost", "8086", 'root', 'root', "speedtest")
    client.write_points([{
        "measurement" : "speeds",
        "tags" : {"host" : "laptop"},
        "time" : data['timestamp'],
        "fields" : {
        "ping" : data['ping'],
        "download" : data['download'],
        "upload" : data['upload']
        }}])

import time

while True:
    main()
    time.sleep(60 * 15)

